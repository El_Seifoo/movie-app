package com.seifo.seifomovies.view_model.movies

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.seifo.seifomovies.R
import com.seifo.seifomovies.mapper.asMoviesList
import com.seifo.seifomovies.mapper.asPersonList
import com.seifo.seifomovies.mapper.asSeriesList
import com.seifo.seifomovies.model.model.Movie
import com.seifo.seifomovies.model.remote.ApiListResponse
import com.seifo.seifomovies.model.remote.RemoteMovie
import com.seifo.seifomovies.model.remote.RemotePerson
import com.seifo.seifomovies.model.remote.RemoteSeries
import com.seifo.seifomovies.repository.MoviesRepository
import com.seifo.seifomovies.utils.Resource
import kotlinx.coroutines.launch
import java.io.IOException
import java.lang.Exception

class MoviesViewModel(
    private val flag: Int,
    private val app: Application,
    private val repository: MoviesRepository
) :
    AndroidViewModel(app) {

    private val _movies = MutableLiveData<Resource>()
    val movies: LiveData<Resource>
        get() = _movies

    private var moviesResponse: ApiListResponse<Movie>? = null
    private var page = 1
    private var totalPages = 2

    init {
        getMovies()
    }

    private fun getMovies() = viewModelScope.launch {
        if (page <= totalPages) {
            try {
                _movies.value = Resource.Loading
                val apiResponse = repository.getMovies(page, "en", flag)
                _movies.value = handleApiResponse(apiResponse)
            } catch (e: Exception) {
                Log.e("exception", e.message.toString())
                _movies.value = Resource.Failed(catchNetworkException(e))
            }
        }
    }

    private fun handleApiResponse(apiResponse: ApiListResponse<RemoteMovie>): Resource {
        if (apiResponse.totalPages != null)
            totalPages = apiResponse.totalPages

        return if (apiResponse.statusCode != null) {
            Resource.Failed(catchApiStatusCode(apiResponse.statusCode))
        } else {
            return if (apiResponse.data.isNullOrEmpty()) {
                Resource.Empty(app.resources.getString(R.string.no_available_movies_now))
            } else {
                page++
//                if (moviesResponse == null) {
//                    moviesResponse = apiResponse.asMoviesList()
//                } else {
//                    val oldMovies = moviesResponse?.data
//                    val newMovies = apiResponse.asMoviesList().data
//                    oldMovies?.addAll(newMovies!!)
//                }
                Resource.Success(apiResponse.asMoviesList())
            }
        }
    }

    private fun catchApiStatusCode(statusCode: Int) = when (statusCode) {
        9 -> app.resources.getString(R.string.service_offline)
        7, 11 -> app.resources.getString(R.string.something_went_wrong)
        34 -> app.resources.getString(R.string.not_found)
        else -> app.resources.getString(R.string.something_went_wrong)
    }.plus("\n").plus(app.resources.getString(R.string.tap_to_retry))


    private fun catchNetworkException(e: Exception) = when (e) {
        is IOException -> app.resources.getString(R.string.no_internet_connection)
        else -> app.resources.getString(R.string.something_went_wrong)
    }.plus("\n").plus(app.resources.getString(R.string.tap_to_retry))

    fun onReloadClicked() {
        getMovies()
    }
}