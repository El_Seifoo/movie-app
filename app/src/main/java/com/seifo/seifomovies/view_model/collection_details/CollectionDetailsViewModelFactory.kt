package com.seifo.seifomovies.view_model.collection_details

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.seifo.seifomovies.repository.CollectionDetailsRepository

class CollectionDetailsViewModelFactory(
    private val collectionId: Int,
    private val app: Application,
    private val repository: CollectionDetailsRepository
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CollectionDetailsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CollectionDetailsViewModel(collectionId, app, repository) as T
        }
        throw IllegalArgumentException("Unable to construct viewmodel")
    }
}