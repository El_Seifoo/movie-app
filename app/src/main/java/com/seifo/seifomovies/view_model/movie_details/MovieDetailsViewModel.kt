package com.seifo.seifomovies.view_model.movie_details

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.seifo.seifomovies.repository.MovieDetailsRepository
import kotlinx.coroutines.launch

class MovieDetailsViewModel(
    private val movieId: Int,
    private val app: Application,
    private val repository: MovieDetailsRepository
) :
    AndroidViewModel(app) {

    val movieDetails = repository.movieDetails
    val similarMovies = repository.similarMovies
    val cast = repository.cast
    val reviews = repository.reviews

    init {
        viewModelScope.launch {
            repository.fetchMovieDetails(movieId)
            repository.fetchSimilarMovies(movieId)
            repository.fetchCast(movieId)
            repository.fetchReviews(movieId)
        }
    }


    fun onSimilarMoviesShowAllClickListener(movieId: Int) {
        Toast.makeText(app, "showAll: $movieId", Toast.LENGTH_SHORT).show()
    }
}