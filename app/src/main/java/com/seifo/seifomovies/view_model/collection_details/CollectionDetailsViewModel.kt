package com.seifo.seifomovies.view_model.collection_details

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.seifo.seifomovies.model.model.CollectionDetails
import com.seifo.seifomovies.repository.CollectionDetailsRepository
import kotlinx.coroutines.launch

class CollectionDetailsViewModel(
    private val movieId: Int,
    private val app: Application,
    private val repository: CollectionDetailsRepository
) :
    AndroidViewModel(app) {

    private val _collectionDetails = repository.collectionDetails
    val collectionDetails: LiveData<CollectionDetails>
        get() = _collectionDetails

    init {
        viewModelScope.launch {
            repository.fetchCollectionDetails(movieId)
        }
    }

}