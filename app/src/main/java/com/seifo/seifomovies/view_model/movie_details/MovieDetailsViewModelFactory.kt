package com.seifo.seifomovies.view_model.movie_details

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.seifo.seifomovies.repository.MovieDetailsRepository

class MovieDetailsViewModelFactory(
    private val movieId: Int,
    private val app: Application,
    private val repository: MovieDetailsRepository
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MovieDetailsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return MovieDetailsViewModel(movieId, app, repository) as T
        }
        throw IllegalArgumentException("Unable to construct viewmodel")
    }
}