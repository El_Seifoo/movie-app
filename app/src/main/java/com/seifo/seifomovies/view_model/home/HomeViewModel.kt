package com.seifo.seifomovies.view_model.home

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.seifo.seifomovies.R
import com.seifo.seifomovies.mapper.asMoviesList
import com.seifo.seifomovies.mapper.asPersonList
import com.seifo.seifomovies.mapper.asSeriesList
import com.seifo.seifomovies.model.remote.ApiListResponse
import com.seifo.seifomovies.model.remote.RemoteMovie
import com.seifo.seifomovies.model.remote.RemotePerson
import com.seifo.seifomovies.model.remote.RemoteSeries
import com.seifo.seifomovies.repository.HomeRepository
import com.seifo.seifomovies.utils.Resource
import kotlinx.coroutines.*
import java.io.IOException
import java.lang.Exception

class HomeViewModel(private val app: Application, private val repository: HomeRepository) :
    AndroidViewModel(app) {

    private val _popularMovies = MutableLiveData<Resource>()
    val popularMovies: LiveData<Resource>
        get() = _popularMovies

    private val _upComingMovies = MutableLiveData<Resource>()
    val upComingMovies: LiveData<Resource>
        get() = _upComingMovies

    private val _onAirSeries = MutableLiveData<Resource>()
    val onAirSeries: LiveData<Resource>
        get() = _onAirSeries

    private val _popularSeries = MutableLiveData<Resource>()
    val popularSeries: LiveData<Resource>
        get() = _popularSeries

    private val _popularPersons = MutableLiveData<Resource>()
    val popularPersons: LiveData<Resource>
        get() = _popularPersons


    init {
        getPopularMovies()
        getUpComingMovies()
        getOnAirSeries()
        getPopularSeries()
        getPopularPersons()
    }

    private fun getPopularMovies() = viewModelScope.launch {
        try {
            _popularMovies.value = Resource.Loading
            val apiResponse = repository.getPopularMovies()
            _popularMovies.value = handleApiResponse(apiResponse, 0)
        } catch (e: Exception) {
            Log.e("exception", e.message.toString())
            _popularMovies.value = Resource.Failed(catchNetworkException(e))
        }
    }

    private fun getUpComingMovies() = viewModelScope.launch {
        try {
            _upComingMovies.value = Resource.Loading
            val apiResponse = repository.getUpComingMovies()
            _upComingMovies.value = handleApiResponse(apiResponse, 0)
        } catch (e: Exception) {
            Log.e("exception", e.message.toString())
            _upComingMovies.value = Resource.Failed(catchNetworkException(e))
        }
    }

    private fun getOnAirSeries() = viewModelScope.launch {
        try {
            _onAirSeries.value = Resource.Loading
            val apiResponse = repository.getOnAirSeries()
            _onAirSeries.value = handleApiResponse(apiResponse, 1)
        } catch (e: Exception) {
            Log.e("exception", e.message.toString())
            _onAirSeries.value = Resource.Failed(catchNetworkException(e))
        }
    }

    private fun getPopularSeries() = viewModelScope.launch {
        try {
            _popularSeries.value = Resource.Loading
            val apiResponse = repository.getPopularSeries()
            _popularSeries.value = handleApiResponse(apiResponse, 1)
        } catch (e: Exception) {
            Log.e("exception", e.message.toString())
            _popularSeries.value = Resource.Failed(catchNetworkException(e))
        }
    }

    private fun getPopularPersons() = viewModelScope.launch {
        try {
            _popularPersons.value = Resource.Loading
            val apiResponse = repository.getPopularPersons()
            _popularPersons.value = handleApiResponse(apiResponse, 2)
        } catch (e: Exception) {
            Log.e("exception", e.message.toString())
            _popularPersons.value = Resource.Failed(catchNetworkException(e))
        }
    }

    /*
        handle all api Response
        @flag : flag to know which api response is being handled
        0 -> movies api (popular , upcoming)
        1 -> series api (popular , onAir)
        2 -> persons api (popular)
        how response is been handled
        first check if there is status code
        if y : handle api status codes
        if n : check if list is null or empty
        if y : display message that list is empty
        if n : display the list
     */
    private fun <T> handleApiResponse(apiResponse: ApiListResponse<T>, flag: Int): Resource {
        return if (apiResponse.statusCode != null) {
            Resource.Failed(catchApiStatusCode(apiResponse.statusCode))
        } else {
            return if (apiResponse.data.isNullOrEmpty()) {
                Resource.Empty(
                    when (flag) {
                        0 -> app.resources.getString(R.string.no_available_movies_now)
                        1 -> app.resources.getString(R.string.no_available_series_now)
                        else -> app.resources.getString(R.string.no_available_persons_now)
                    }
                )
            } else {
                Resource.Success(
                    when (flag) {
                        0 -> (apiResponse as ApiListResponse<RemoteMovie>).asMoviesList()
                        1 -> (apiResponse as ApiListResponse<RemoteSeries>).asSeriesList()
                        else -> (apiResponse as ApiListResponse<RemotePerson>).asPersonList()
                    }
                )
            }
        }
    }


    private fun catchNetworkException(e: Exception) = when (e) {
        is IOException -> app.resources.getString(R.string.no_internet_connection)
        else -> app.resources.getString(R.string.something_went_wrong)
    }.plus("\n").plus(app.resources.getString(R.string.tap_to_retry))

    private fun catchApiStatusCode(statusCode: Int) = when (statusCode) {
        9 -> app.resources.getString(R.string.service_offline)
        7, 11 -> app.resources.getString(R.string.something_went_wrong)
        34 -> app.resources.getString(R.string.not_found)
        else -> app.resources.getString(R.string.something_went_wrong)
    }.plus("\n").plus(app.resources.getString(R.string.tap_to_retry))

    /*
        Retry requesting api
     */
    fun onReloadClicked(flag: Int) {
        Log.e("flag", flag.toString())
        when (flag) {
            0 -> getPopularMovies()
            1 -> getUpComingMovies()
            2 -> getOnAirSeries()
            3 -> getPopularSeries()
            else -> getPopularPersons()
        }
    }


}