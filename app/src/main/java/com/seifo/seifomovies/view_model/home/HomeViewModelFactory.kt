package com.seifo.seifomovies.view_model.home

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.seifo.seifomovies.repository.HomeRepository

class HomeViewModelFactory(private val app: Application,private val repository: HomeRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return HomeViewModel(app,repository) as T
        }
        throw IllegalArgumentException("Unable to construct viewmodel")
    }
}