package com.seifo.seifomovies.view_model.movies

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.seifo.seifomovies.repository.MoviesRepository

class MoviesViewModelFactory(
    private val flag: Int,
    private val app: Application,
    private val repository: MoviesRepository
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MoviesViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return MoviesViewModel(flag, app, repository) as T
        }
        throw IllegalArgumentException("Unable to construct viewmodel")
    }
}