package com.seifo.seifomovies.utils

sealed class Resource {
    data class Success<T>(val data: T) : Resource()
    data class Failed(val message: String) : Resource()
    object Loading : Resource()
    data class Empty(val message: String) : Resource()
}
