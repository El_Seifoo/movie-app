package com.seifo.seifomovies.utils

import android.annotation.SuppressLint
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.Group
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.seifo.seifomovies.R
import com.seifo.seifomovies.adapter.*
import com.seifo.seifomovies.model.model.*
import com.seifo.seifomovies.model.remote.ApiListResponse


@BindingAdapter("img_url", "img_size")
fun ImageView.loadImage(url: String?, size: String) {
    Glide.with(this.context)
        .load(IMAGES_BASE_URL + size + url)
        .apply(
            RequestOptions.placeholderOf(android.R.drawable.stat_sys_upload)
                .error(android.R.drawable.stat_notify_error)
                .centerInside()
                .dontAnimate()
                .dontTransform()
        )
        .into(this)
}

/*
    decides the visibility of popularity group (movie details fragment - person item)
 */
@BindingAdapter("popularity_group_visibility")
fun Group.setGroupVisibility(popularity: Float?) {
    when (popularity) {
        null -> this.visibility = View.GONE
        0f -> this.visibility = View.GONE
        else -> this.visibility = View.VISIBLE
    }
}

/*
    decide the visibility of movie status group (movie details fragment)
 */
@BindingAdapter("movie_status_visibility")
fun Group.setGroupVisibility(value: String?) {
    if (value.isNullOrEmpty())
        this.visibility = View.GONE
    else
        this.visibility = View.VISIBLE
}

/*
    remove .0* from popularity then convert to string
 */
@BindingAdapter("popularity")
fun TextView.setPopularity(popularity: Float?) {
    when (popularity) {
        null -> return
        0f -> return
        else -> {
            when (popularity % 1) {
                0f -> this.text = popularity.toInt().toString()
                else -> this.text = popularity.toString()
            }
        }
    }

}

/*
    decide the visibility of rate and vote count Group (movie details fragment)
 */
@BindingAdapter("rate", "vote_count")
fun Group.setRateGroupVisibility(rate: Float, voteCount: Int) {
    if (rate == 0f && voteCount == 0)
        this.visibility = View.GONE
    else
        this.visibility = View.VISIBLE
}

/*
    decide the visibility of rate, popularity and status FrameLayout (movie details fragment)
 */
@BindingAdapter("rate", "vote_count", "popularity")
fun FrameLayout.setFrameVisibility(rate: Float, voteCount: Int, popularity: Float?) {
    if (rate == 0f && voteCount == 0 && popularity == null)
        this.visibility = View.GONE
    else if (rate == 0f && voteCount == 0 && popularity == 0f)
        this.visibility = View.GONE
    else
        this.visibility = View.VISIBLE
}

/*
                (for only string values)
    decide TextView visibility depending on string nullability
 */
@BindingAdapter("string_value_with_text_visibility")
fun TextView.setTextAndVisibility(text: String?) {
    if (text.isNullOrEmpty()) this.visibility = View.GONE
    else {
        this.text = text
        this.visibility = View.VISIBLE
    }
}

/*
    convert Int to String
 */
@BindingAdapter("int_value")
fun TextView.convertIntToString(value: Int) {
    this.text = value.toString()
}

/*
    1- decide text color depending on rate range
    2- round rate to 1 decimal number
    3- remove 0.* and convert rate to string
 */
@BindingAdapter("rate")
fun TextView.setRateTextAndColor(rate: Float) {
    this.setTextColor(
        ContextCompat.getColor(
            this.context,
            when {
                rate < 4 -> R.color.red
                rate < 8 -> R.color.yellow
                else -> R.color.green
            }
        )
    )

    val roundedRate = String.format("%.1f", rate).toFloat()
    if (roundedRate % 1 == 0f)
        this.text = roundedRate.toInt().toString()
    else
        this.text = roundedRate.toString()
}

/*
    shelha wy esta5dem el group zy movie details fragment ........
 */
@BindingAdapter("rate_with_visibility")
fun View.setRateTextAndColorAndVisibility(rate: Float) {
    if (rate == 0f)
        this.visibility = View.GONE
    else {
        this.visibility = View.VISIBLE

        if (this is TextView) {
            this.setTextColor(
                ContextCompat.getColor(
                    this.context,
                    when {
                        rate < 4 -> R.color.red
                        rate < 8 -> R.color.yellow
                        else -> R.color.green
                    }
                )
            )
            val roundedRate = String.format("%.1f", rate).toFloat()
            if (roundedRate % 1 == 0f)
                this.text = roundedRate.toInt().toString()
            else
                this.text = roundedRate.toString()
        }


    }


}

//2017-02-18T13:49:12.341Z
@BindingAdapter("date_with_T", "is_short")
fun TextView.formatDate(date: String?, isShort: Boolean) {
    if (date.isNullOrEmpty()) {
        this.visibility = View.GONE
        return
    }

    val splitDate = date.split("T")
    this.formatDateWithVisibility(splitDate[0], isShort)
}

/*
    decide the visibility of the TextView and set date formatted to the TextView
    is_short -> decides Month format i.e  short -> Jan , full -> January
 */
@SuppressLint("SetTextI18n")
@BindingAdapter("date_format_with_visibility", "is_short")
fun TextView.formatDateWithVisibility(date: String?, isShort: Boolean) {
    if (date.isNullOrEmpty()) {
        this.visibility = View.GONE
        return
    }

    val splitDate = date.split("-")
    val months = this.context.resources.getStringArray(
        if (isShort) R.array.short_months
        else R.array.full_months
    )

    this.visibility = View.VISIBLE
    this.text =
        "${months[splitDate[1].toInt() - 1]} ${if (isShort) ",${splitDate[0]}" else "${splitDate[2]},${splitDate[0]}"}"
}

/*
    decide the visibility of genres TextView depending on list nullability and size
    then if not null and not empty set all genres formatted in one text to the TextView
 */
@BindingAdapter("genres_list_with_visibility")
fun TextView.setGenres(genres: List<Genre>?) {
    if (genres.isNullOrEmpty()) {
        this.visibility = View.GONE
        return
    }

    this.visibility = View.VISIBLE
    var genreString = ""
    for (index in genres.indices) {
        genreString += genres[index].name

        if (index < genres.lastIndex)
            genreString += ", "
    }

    this.text = genreString
}

/*
    decide the visibility of movie length TextView depending on val nullability 
    then format movie length and set it to the TextView if not null
 */
@SuppressLint("SetTextI18n")
@BindingAdapter("movie_length_with_visibility")
fun TextView.setMovieLength(length: Int?) {
    if (length == null) {
        this.visibility = View.GONE
        return
    }
    if (length == 0) {
        this.visibility = View.GONE
        return
    }

    this.visibility = View.VISIBLE
    val hours = length / 60
    val minutes = length % 60

    val hoursString = when {
        hours == 0 -> ""
        hours < 10 -> "0$hours${this.context.getText(R.string.hours_short)}"
        else -> "$hours${this.context.getText(R.string.hours_short)}"
    }

    val minutesString = when {
        minutes == 0 -> ""
        minutes < 10 -> " 0$minutes${this.context.getText(R.string.minutes_short)}"
        else -> " $minutes${this.context.getText(R.string.minutes_short)}"
    }

    this.text = hoursString + minutesString
}

/*
    decide the visibility of collection group (movie details fragment)
 */
@BindingAdapter("collection")
fun Group.setVisibility(collection: MovieCollection?) {
    if (collection == null)
        this.visibility = View.GONE
    else
        this.visibility = View.VISIBLE
}

/*
    decide all lists groups visibility depending on list nullability and size
 */
@BindingAdapter("visibility_by_list")
fun <T> Group.setVisibility(list: List<T>?) {
    if (list.isNullOrEmpty())
        this.visibility = View.GONE
    else
        this.visibility = View.VISIBLE
}

/*
    check api response status (success , failed or loading)
    to handle the visibility of progressBar
 */
@BindingAdapter("api_status")
fun ProgressBar.setProgress(resource: Resource?) {
    if (resource != null) {
        when (resource) {
            is Resource.Loading -> this.visibility = View.VISIBLE
            is Resource.Success<*>, is Resource.Failed, is Resource.Empty -> this.visibility =
                View.GONE
        }
    }
}

/*
    check api response status (success , failed or loading)
    to handle the visibility of progressBar
 */
@BindingAdapter("api_status")
fun TextView.setErrorView(resource: Resource?) {
    if (resource != null) {
        when (resource) {
            is Resource.Loading -> this.visibility = View.GONE
            is Resource.Success<*> -> this.visibility = View.GONE
            is Resource.Failed -> {
                this.visibility = View.VISIBLE
                this.text = resource.message
            }
            is Resource.Empty -> {
                this.visibility = View.VISIBLE
                this.text = resource.message
            }
        }
    }
}

/*
    initialize recyclerView and set its components (adapter and layout manager)
    and adapter clickListener
    @resource : api response with api status
    @clickListener : clickListener of the adapter
    @flag : flag to know which adapter type we handling (MovieAdapter , PersonAdapter or PersonAdapter)
 */
@BindingAdapter("api_status", "click_listener", "flag")
fun RecyclerView.setHorizontalList(
    resource: Resource?,
    clickListener: Any,
    flag: Int
) {
    if (resource != null) {
        when (resource) {
            is Resource.Loading ->
                this.visibility = View.INVISIBLE
            is Resource.Success<*> -> {
                this.visibility = View.VISIBLE
                val adapter = getOrSetupRecViewComponents(this, clickListener, flag)
                when (flag) {
                    0 -> {
                        val list = (resource.data as ApiListResponse<Movie>).data as List<Movie>
                        (adapter as MoviesAdapter).submitList(list)
                    }
                    1 -> {
                        val list = (resource.data as ApiListResponse<Series>).data as List<Series>
                        (adapter as SeriesAdapter).submitList(list)
                    }
                    else -> {
                        val list = (resource.data as ApiListResponse<Person>).data as List<Person>
                        (adapter as PersonAdapter).submitList(list)
                    }
                }
            }
            is Resource.Failed, is Resource.Empty ->
                this.visibility = View.INVISIBLE
        }
    }
}

/*
    check if recyclerView has adapter or not
    if y : return the same adapter
    if n : create new one
 */
private fun getOrSetupRecViewComponents(
    recyclerView: RecyclerView,
    clickListener: Any,
    flag: Int
): Any {
    return if (recyclerView.adapter != null) {
        when (flag) {
            0 -> recyclerView.adapter as MoviesAdapter
            1 -> recyclerView.adapter as SeriesAdapter
            else -> recyclerView.adapter as PersonAdapter
        }
    } else {
        val adapter = when (flag) {
            0 -> MoviesAdapter(clickListener as OnMovieClickListener)
            1 -> SeriesAdapter(clickListener as OnSeriesItemClickListener)
            else -> PersonAdapter(clickListener as OnPersonClickListener)
        }
        recyclerView.layoutManager = getOrCreateLayoutManager(recyclerView)
        recyclerView.adapter = adapter
        adapter

    }
}

/*
    check if recyclerView has layoutManager or not
    if y : return the same layoutManager
    if n : create new one
 */
private fun getOrCreateLayoutManager(recyclerView: RecyclerView): LinearLayoutManager {
    return if (recyclerView.layoutManager != null) {
        recyclerView.layoutManager as LinearLayoutManager
    } else {
        LinearLayoutManager(recyclerView.context, LinearLayoutManager.HORIZONTAL, false)
    }
}


/*
    initialize recyclerView and set its components (adapter and layout manager)
    and adapter clickListener
    @resource : api response with api status
    @clickListener : clickListener of the adapter
    @flag : flag to know which adapter type we handling (MovieAdapter , PersonAdapter or PersonAdapter)
 */
@BindingAdapter("vertical_api_status", "vertical_click_listener", "vertical_flag")
fun RecyclerView.setVerticalList(
    resource: Resource?,
    clickListener: Any,
    flag: Int
) {
    if (resource != null) {
        when (resource) {
            is Resource.Loading ->
                this.visibility = View.INVISIBLE
            is Resource.Success<*> -> {
                this.visibility = View.VISIBLE
                val adapter = getOrSetupVerticalRecViewComponents(this, clickListener, flag)
                when (flag) {
                    0 -> {
                        val list = (resource.data as ApiListResponse<Movie>).data as List<Movie>
                        (adapter as MoviesAdapter).submitList(list)
                    }
                    1 -> {
                        val list = (resource.data as ApiListResponse<Series>).data as List<Series>
                        (adapter as SeriesAdapter).submitList(list)
                    }
                    else -> {
                        val list = (resource.data as ApiListResponse<Person>).data as List<Person>
                        (adapter as PersonAdapter).submitList(list)
                    }
                }
            }
            is Resource.Failed, is Resource.Empty ->
                this.visibility = View.INVISIBLE
        }
    }
}


/*
    check if recyclerView has adapter or not
    if y : return the same adapter
    if n : create new one
 */
private fun getOrSetupVerticalRecViewComponents(
    recyclerView: RecyclerView,
    clickListener: Any,
    flag: Int
): Any {
    return if (recyclerView.adapter != null) {
        when (flag) {
            0 -> recyclerView.adapter as MoviesAdapter
            1 -> recyclerView.adapter as SeriesAdapter
            else -> recyclerView.adapter as PersonAdapter
        }
    } else {
        val adapter = when (flag) {
            0 -> MoviesAdapter(clickListener as OnMovieClickListener)
            1 -> SeriesAdapter(clickListener as OnSeriesItemClickListener)
            else -> PersonAdapter(clickListener as OnPersonClickListener)
        }
        recyclerView.layoutManager = getOrCreateVerticalLayoutManager(recyclerView)
        recyclerView.adapter = adapter
        adapter

    }
}

/*
    check if recyclerView has layoutManager or not
    if y : return the same layoutManager
    if n : create new one
 */
private fun getOrCreateVerticalLayoutManager(recyclerView: RecyclerView): LinearLayoutManager {
    return if (recyclerView.layoutManager != null) {
        recyclerView.layoutManager as LinearLayoutManager
    } else {
        LinearLayoutManager(recyclerView.context, LinearLayoutManager.VERTICAL, false)
    }
}


@BindingAdapter("movies_fragment_title")
fun TextView.setText(flag:Int){
    this.setText(if (flag == 0) this.context.getString(R.string.popular_movies) else this.context.getString(R.string.upcoming_movies))
}





