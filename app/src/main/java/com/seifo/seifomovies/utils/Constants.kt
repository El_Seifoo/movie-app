package com.seifo.seifomovies.utils


public const val API_KEY = "d085df37664abde344bb2141cac5e951"
public const val BASE_URL = "https://api.themoviedb.org/3/"
public const val IMAGES_BASE_URL = "https://image.tmdb.org/t/p/"
public const val POPULAR_MOVIES = "movie/popular"
public const val UPCOMING_MOVIES = "movie/upcoming"
public const val ON_AIR_SERIES = "tv/on_the_air"
public const val POPULAR_SERIES = "tv/popular"
public const val POPULAR_PERSON = "person/popular"
public const val MOVIE_DETAILS = "movie/"
public const val SIMILAR_MOVIE_1 = "movie/"
public const val SIMILAR_MOVIE_2 = "/similar"
public const val COLLECTION = "collection/"
public const val CAST_CREW_1 = "movie/"
public const val CAST_CREW_2 = "/credits"
public const val MOVIE_REVIEWS_1 = "movie/"
public const val MOVIE_REVIEWS_2 = "/reviews"


// REQUEST QUERIES
public const val PAGE = "page"
public const val LANGUAGE = "language"
public const val _API_KEY = "api_key"
public const val MOVIE_ID = "movie_id"
public const val COLLECTION_ID = "collection_id"


