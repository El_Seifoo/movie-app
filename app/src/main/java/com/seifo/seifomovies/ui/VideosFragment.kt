package com.seifo.seifomovies.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.seifo.seifomovies.R
import com.seifo.seifomovies.databinding.FragmentVideosBinding

class VideosFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentVideosBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_videos, container, false)

        return binding.root
    }
}