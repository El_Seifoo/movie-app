package com.seifo.seifomovies.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.seifo.seifomovies.adapter.MoviesAdapter
import com.seifo.seifomovies.adapter.OnMovieClickListener
import com.seifo.seifomovies.databinding.FragmentCollectionDetailsBinding
import com.seifo.seifomovies.repository.CollectionDetailsRepository
import com.seifo.seifomovies.view_model.collection_details.CollectionDetailsViewModel
import com.seifo.seifomovies.view_model.collection_details.CollectionDetailsViewModelFactory

class CollectionDetailsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentCollectionDetailsBinding.inflate(inflater, container, false)

        val viewModelFactory =
            CollectionDetailsViewModelFactory(
                CollectionDetailsFragmentArgs.fromBundle(requireArguments()).collectionId,
                requireActivity().application,
                CollectionDetailsRepository()
            )

        val viewModel =
            ViewModelProvider(this, viewModelFactory).get(CollectionDetailsViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        val partsAdapter = MoviesAdapter(OnMovieClickListener {
            findNavController().navigate(
                CollectionDetailsFragmentDirections.actionCollectionDetailsFragmentToMovieFragment(
                    it.id
                )
            )
        })

        binding.partsList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.partsList.adapter = partsAdapter

        viewModel.collectionDetails.observe(viewLifecycleOwner, Observer {
            partsAdapter.submitList(it?.movies)
            binding.collection = it
        })



        return binding.root

    }
}