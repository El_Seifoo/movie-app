package com.seifo.seifomovies.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.seifo.seifomovies.R
import com.seifo.seifomovies.adapter.OnMovieClickListener
import com.seifo.seifomovies.databinding.FragmentMoviesBinding
import com.seifo.seifomovies.repository.MoviesRepository
import com.seifo.seifomovies.view_model.movies.MoviesViewModel
import com.seifo.seifomovies.view_model.movies.MoviesViewModelFactory

class MoviesFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentMoviesBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_movies, container, false)

        val flag = MoviesFragmentArgs.fromBundle(requireArguments()).flag
        binding.flag = flag

        val viewModelFactory =
            MoviesViewModelFactory(
                flag,
                requireActivity().application,
                MoviesRepository()
            )
        val viewModel = ViewModelProvider(this, viewModelFactory).get(MoviesViewModel::class.java)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this


        val movieClickListener = OnMovieClickListener {
            findNavController().navigate(
                MoviesFragmentDirections.actionMoviesFragmentToMovieFragment(
                    it.id
                )
            )
        }
        binding.clickListener = movieClickListener

        return binding.root
    }
}