package com.seifo.seifomovies.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.seifo.seifomovies.databinding.FragmentReviewDetailsBinding

class ReviewDetailsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentReviewDetailsBinding.inflate(inflater, container, false)

        binding.review = ReviewDetailsFragmentArgs.fromBundle(requireArguments()).review

        return binding.root
    }
}