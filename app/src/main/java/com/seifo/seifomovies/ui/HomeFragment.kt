package com.seifo.seifomovies.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.seifo.seifomovies.R
import com.seifo.seifomovies.adapter.*
import com.seifo.seifomovies.databinding.FragmentHomeBinding
import com.seifo.seifomovies.repository.HomeRepository
import com.seifo.seifomovies.view_model.home.HomeViewModel
import com.seifo.seifomovies.view_model.home.HomeViewModelFactory

class HomeFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentHomeBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)


        val viewModelFactory = HomeViewModelFactory(requireActivity().application, HomeRepository())
        val viewModel = ViewModelProvider(this, viewModelFactory).get(HomeViewModel::class.java)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this


        val movieClickListener = OnMovieClickListener {
            this.findNavController().navigate(
                HomeFragmentDirections.actionHomeFragmentToMovieFragment(it.id)
            )
        }
        binding.movieClickListener = movieClickListener

        val seriesClickListener = OnSeriesItemClickListener {
            Toast.makeText(context, it.title, Toast.LENGTH_SHORT).show()
        }
        binding.seriesClickListener = seriesClickListener

        val personClickListener = OnPersonClickListener {
            Toast.makeText(context, it.name, Toast.LENGTH_SHORT).show()
        }
        binding.personClickListener = personClickListener

        binding.showAllPopularMovies.setOnClickListener {
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToMoviesFragment(0))
        }

        binding.showAllUpcomingMovies.setOnClickListener {
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToMoviesFragment(1))
        }
        return binding.root
    }


}