package com.seifo.seifomovies.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.seifo.seifomovies.adapter.*
import com.seifo.seifomovies.databinding.MovieDetailsFragmentBinding
import com.seifo.seifomovies.repository.MovieDetailsRepository
import com.seifo.seifomovies.view_model.movie_details.MovieDetailsViewModel
import com.seifo.seifomovies.view_model.movie_details.MovieDetailsViewModelFactory

class MovieDetailsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = MovieDetailsFragmentBinding.inflate(inflater, container, false)


        val viewModelFactory =
            MovieDetailsViewModelFactory(
                MovieDetailsFragmentArgs.fromBundle(requireArguments()).movieId,
                requireActivity().application,
                MovieDetailsRepository()
            )

        val viewModel =
            ViewModelProvider(this, viewModelFactory).get(MovieDetailsViewModel::class.java)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.movieDetails.observe(viewLifecycleOwner, Observer {
            it.let {
                binding.movieDetails = it
            }
        })

        val similarMoviesAdapter = MoviesAdapter(OnMovieClickListener {
            findNavController().navigate(MovieDetailsFragmentDirections.actionMovieFragmentSelf(it.id))
        })

        viewModel.similarMovies.observe(viewLifecycleOwner, Observer {
            it.let {
                similarMoviesAdapter.submitList(it)
            }
        })

        binding.similarMoviesList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.similarMoviesList.adapter = similarMoviesAdapter


        val castAdapter = CastAdapter(OnCastClickListener {
            Toast.makeText(requireContext(), it.name, Toast.LENGTH_SHORT).show()
        })

        viewModel.cast.observe(viewLifecycleOwner, Observer {
            it.let {
                castAdapter.submitList(it)
            }
        })

        binding.castList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.castList.adapter = castAdapter


        val reviewsAdapter = ReviewsAdapter(OnReviewClickListener {
            findNavController().navigate(
                MovieDetailsFragmentDirections.actionMovieFragmentToReviewDetailsFragment(
                    it
                )
            )
        })

        viewModel.reviews.observe(viewLifecycleOwner, Observer {
            reviewsAdapter.submitList(it)
        })

        binding.reviewsList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.reviewsList.adapter = reviewsAdapter

        binding.collectionFrame.setOnClickListener {
            findNavController().navigate(
                MovieDetailsFragmentDirections.actionMovieFragmentToCollectionDetailsFragment(
                    binding.movieDetails?.collection?.id ?: 0
                )
            )
        }
        return binding.root
    }


}

