package com.seifo.seifomovies.retrofit

import com.seifo.seifomovies.model.remote.*
import com.seifo.seifomovies.utils.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


fun httpClient(): OkHttpClient.Builder {

    var logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY
    var httpClient = OkHttpClient.Builder()

//    if (BuildConfig.DEBUG)
    httpClient.addInterceptor(logging)

    return httpClient
}


private var retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create())
    .baseUrl(BASE_URL)
    .client(httpClient().build())
    .build()

object AppApi {
    val apiService: ApiService by lazy {
        retrofit.create(ApiService::class.java)
    }
}

interface ApiService {
    @GET(POPULAR_MOVIES)
    suspend fun getPopularMovies(
        @Query(PAGE) page: Int,
        @Query(LANGUAGE) lang: String,
        @Query(_API_KEY) apiKey: String = API_KEY
    ): ApiListResponse<RemoteMovie>

    @GET(UPCOMING_MOVIES)
    suspend fun getUpComingMovies(
        @Query(PAGE) page: Int,
        @Query(LANGUAGE) lang: String,
        @Query(_API_KEY) apiKey: String = API_KEY
    ): ApiListResponse<RemoteMovie>

    @GET(ON_AIR_SERIES)
    suspend fun getOnAirSeries(
        @Query(PAGE) page: Int,
        @Query(LANGUAGE) lang: String,
        @Query(_API_KEY) apiKey: String = API_KEY
    ): ApiListResponse<RemoteSeries>

    @GET(POPULAR_SERIES)
    suspend fun getPopularSeries(
        @Query(PAGE) page: Int,
        @Query(LANGUAGE) lang: String,
        @Query(_API_KEY) apiKey: String = API_KEY
    ): ApiListResponse<RemoteSeries>

    @GET(POPULAR_PERSON)
    suspend fun getPopularPersons(
        @Query(PAGE) page: Int,
        @Query(LANGUAGE) lang: String,
        @Query(_API_KEY) apiKey: String = API_KEY
    ): ApiListResponse<RemotePerson>

    @GET("$MOVIE_DETAILS{movie_id}")
    suspend fun getMovieDetails(
        @Path(MOVIE_ID) movieId: Int,
        @Query(LANGUAGE) lang: String,
        @Query(_API_KEY) apiKey: String = API_KEY
    ): RemoteMovieDetails

    @GET("$SIMILAR_MOVIE_1{movie_id}$SIMILAR_MOVIE_2")
    suspend fun getSimilarMovies(
        @Path(MOVIE_ID) movieId: Int,
        @Query(PAGE) page: Int,
        @Query(LANGUAGE) lang: String,
        @Query(_API_KEY) apiKey: String = API_KEY
    ): ApiListResponse<RemoteMovie>

    @GET("$COLLECTION{collection_id}")
    suspend fun getCollectionDetails(
        @Path(COLLECTION_ID) collection: Int,
        @Query(LANGUAGE) lang: String,
        @Query(_API_KEY) apiKey: String = API_KEY
    ): RemoteCollectionDetails

    @GET("$CAST_CREW_1{movie_id}$CAST_CREW_2")
    suspend fun getMovieCast(
        @Path(MOVIE_ID) collection: Int,
        @Query(LANGUAGE) lang: String,
        @Query(_API_KEY) apiKey: String = API_KEY
    ): ApiListResponse<RemoteCast>

    @GET("$MOVIE_REVIEWS_1{movie_id}$MOVIE_REVIEWS_2")
    suspend fun getMovieReview(
        @Path(MOVIE_ID) collection: Int,
        @Query(PAGE) page: Int,
        @Query(LANGUAGE) lang: String,
        @Query(_API_KEY) apiKey: String = API_KEY
    ): ApiListResponse<RemoteReview>


}