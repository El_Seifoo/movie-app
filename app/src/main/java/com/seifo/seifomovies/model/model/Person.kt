package com.seifo.seifomovies.model.model


class Person(
    val id: Int,
    val name: String,
    val department: String?,
    val popularity: Float?,
    val photo: String?,
    val gender: Int?
)