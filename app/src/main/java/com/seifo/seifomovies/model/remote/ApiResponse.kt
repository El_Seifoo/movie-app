package com.seifo.seifomovies.model.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ApiResponse<T>(
    @Json(name = "") val data: T?,
    @Json(name = "status_message") val statusMessage: String?,
    @Json(name = "status_code") val statusCode: String?,
    @Json(name = "success") val isSuccess: String?
)