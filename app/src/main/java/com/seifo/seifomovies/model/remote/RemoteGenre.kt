package com.seifo.seifomovies.model.remote

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class RemoteGenre(val id: Int, val name: String)