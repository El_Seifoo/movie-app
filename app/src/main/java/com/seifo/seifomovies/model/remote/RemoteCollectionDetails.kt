package com.seifo.seifomovies.model.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class RemoteCollectionDetails(
    val id: Int,
    @Json(name = "name") val title: String,
    val overview: String,
    @Json(name = "poster_path") val poster: String?,
    @Json(name = "backdrop_path") val backdrop: String?,
    @Json(name = "parts") val remoteMovies: List<RemoteMovie>
)