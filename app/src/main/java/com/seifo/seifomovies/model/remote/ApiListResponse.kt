package com.seifo.seifomovies.model.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ApiListResponse<T>(
    @Json(name = "results") val data: MutableList<T>?,
    @Json(name = "cast") val data1: List<T>?,
    @Json(name = "total_pages") val totalPages: Int?,
    @Json(name = "status_message") val statusMessage: String?,
    @Json(name = "status_code") val statusCode: Int?,
    @Json(name = "success") val isSuccess: String?
)