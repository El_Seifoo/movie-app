package com.seifo.seifomovies.model.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class RemoteReview(
    val id: String,
    @Json(name = "author") val reviewer: String,
    @Json(name = "author_details") val reviewerDetails: RemoteReviewerDetails?,
    val content: String?,
    @Json(name = "created_at") val createdAt: String?,
    val url: String?
)