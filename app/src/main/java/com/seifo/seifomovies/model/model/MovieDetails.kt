package com.seifo.seifomovies.model.model



class MovieDetails(
    val id: Int,
    val title: String,
    val length: Int?,
    val status: String,
    val tagline: String?,
    val voteCount: Int?,
    val popularity: Float?,
    val budget: Int?,
    val revenue: Int?,
    val genres: List<Genre>?,
    val collection: MovieCollection?,
    val releaseDate: String,
    val poster: String?,
    val originalTitle: String,
    val originalLanguage: String,
    val backDrop: String?,
    val rate: Float,
    val overview: String,
)
