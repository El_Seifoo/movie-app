package com.seifo.seifomovies.model.model


class CollectionDetails(
    val id: Int,
    val title: String,
    val overview: String,
    val poster: String?,
    val backdrop: String?,
    val movies: List<Movie>
)