package com.seifo.seifomovies.model.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class RemoteMovieCollection(
    val id: Int, @Json(name = "name") val title: String,
    @Json(name = "poster_path") val poster: String?
)