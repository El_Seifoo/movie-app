package com.seifo.seifomovies.model.model


class MovieCollection(
    val id: Int,
    val title: String,
    val poster: String?
)