package com.seifo.seifomovies.model.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class RemoteCast(
    val id: Int,
    @Json(name = "cast_id")
    val castId: Int,
    @Json(name = "credit_id")
    val creditId: String,
    val name: String,
    @Json(name = "profile_path")
    val photo: String?,
    val character: String,
)