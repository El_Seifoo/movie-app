package com.seifo.seifomovies.model.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class RemoteSeries(
    val id: Int,
    @Json(name = "name")
    val title: String,
    @Json(name = "poster_path")
    val poster: String?,
    val overview: String,
    @Json(name = "first_air_date")
    val releaseDate: String?,
    @Json(name = "genre_ids")
    val genres: List<Int>,
    @Json(name = "original_name")
    val originalTitle: String,
    @Json(name = "backdrop_path")
    val backDrop: String?,
    @Json(name = "original_language")
    val originalLanguage: String,
    @Json(name = "vote_average")
    val rate: Float
)