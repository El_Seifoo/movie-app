package com.seifo.seifomovies.model.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
class RemoteMovie(
    val id: Int,
    @Json(name = "poster_path")
    val poster: String?,
    val overview: String,
    @Json(name = "release_date")
    val releaseDate: String?,
    @Json(name = "genre_ids")
    val genres: List<Int>,
    @Json(name = "original_title")
    val originalTitle: String,
    val title: String,
    @Json(name = "original_language")
    val originalLanguage: String,
    @Json(name = "backdrop_path")
    val backdrop: String?,
    @Json(name = "vote_average")
    val rate: Float,
)

