package com.seifo.seifomovies.model.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReviewerDetails(
    val name: String?,
    val username: String?,
    val profilePic: String?,
    val rate: Float?
) : Parcelable