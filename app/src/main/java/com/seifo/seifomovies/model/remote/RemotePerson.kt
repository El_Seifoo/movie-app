package com.seifo.seifomovies.model.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class RemotePerson(
    val id: Int,
    val name: String,
    @Json(name = "known_for_department")
    val department: String?,
    val popularity: Float?,
    @Json(name = "profile_path")
    val photo: String?,
    val gender: Int?
)