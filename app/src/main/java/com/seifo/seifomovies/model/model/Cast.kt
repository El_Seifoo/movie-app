package com.seifo.seifomovies.model.model

import com.squareup.moshi.Json

class Cast(
    val id: Int,
    val castId: Int,
    val creditId: String,
    val name: String,
    val photo: String?,
    val character: String,
)