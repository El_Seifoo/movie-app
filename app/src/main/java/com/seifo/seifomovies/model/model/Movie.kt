package com.seifo.seifomovies.model.model

class Movie(
    val id: Int,
    val poster: String?,
    val overview: String,
    val releaseDate: String?,
    val genres: List<Int>,
    val originalTitle: String,
    val title: String,
    val originalLanguage: String,
    val backdrop: String?,
    val rate: Float,
)