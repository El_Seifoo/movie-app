package com.seifo.seifomovies.model.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class RemoteReviewerDetails(
    val name: String?,
    val username: String?,
    @Json(name = "avatar_path")
    val profilePic: String?,
    @Json(name = "rating")
    val rate: Float?
)