package com.seifo.seifomovies.model.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class RemoteMovieDetails(
    val id: Int,
    val title: String,
    @Json(name = "runtime")
    val length: Int?,
    val status: String,
    val tagline: String?,
    @Json(name = "vote_count")
    val voteCount: Int?,
    @Json(name = "popularity")
    val popularity: Float?,
    val budget: Int?,
    val revenue: Int?,
    val remoteGenres: List<RemoteGenre>?,
    @Json(name = "belongs_to_collection")
    val remoteMovieCollection: RemoteMovieCollection?,
    @Json(name = "release_date")
    val releaseDate: String,
    @Json(name = "poster_path")
    val poster: String?,
    @Json(name = "original_title")
    val originalTitle: String,
    @Json(name = "original_language")
    val originalLanguage: String,
    @Json(name = "backdrop_path")
    val backDrop: String?,
    @Json(name = "vote_average")
    val rate: Float,
    val overview: String,
)
