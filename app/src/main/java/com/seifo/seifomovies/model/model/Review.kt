package com.seifo.seifomovies.model.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Review(
    val id: String,
    val reviewer: String,
    val reviewerDetails: ReviewerDetails?,
    val content: String?,
    val createdAt: String?,
    val url: String?
) : Parcelable