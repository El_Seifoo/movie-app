package com.seifo.seifomovies.model.model

import com.squareup.moshi.Json

class Series(
    val id: Int,
    val title: String,
    val poster: String?,
    val overview: String,
    val releaseDate: String?,
    val genres: List<Int>,
    val originalTitle: String,
    val backDrop: String?,
    val originalLanguage: String,
    val rate: Float
)