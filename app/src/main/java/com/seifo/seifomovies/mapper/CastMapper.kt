package com.seifo.seifomovies.mapper

import com.seifo.seifomovies.model.model.Cast
import com.seifo.seifomovies.model.remote.RemoteCast


fun List<RemoteCast>.asCastList(): List<Cast> {
    return this.map {
        Cast(
            id = it.id,
            castId = it.castId,
            creditId = it.creditId,
            name = it.name,
            photo = it.photo,
            character = it.character
        )
    }
}