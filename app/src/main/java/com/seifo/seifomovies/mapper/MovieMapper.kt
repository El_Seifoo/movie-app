package com.seifo.seifomovies.mapper

import com.seifo.seifomovies.model.model.Movie
import com.seifo.seifomovies.model.remote.ApiListResponse
import com.seifo.seifomovies.model.remote.RemoteMovie



fun ApiListResponse<RemoteMovie>.asMoviesList(): ApiListResponse<Movie> {
    return ApiListResponse(
        this.data?.asMoviesList()?.toMutableList(),
        null,
        this.totalPages,
        this.statusMessage,
        this.statusCode,
        this.isSuccess
    )
}


fun List<RemoteMovie>.asMoviesList(): List<Movie> {
    return this.map {
        Movie(
            id = it.id,
            poster = it.poster,
            overview = it.overview,
            releaseDate = it.releaseDate,
            genres = it.genres,
            originalTitle = it.originalTitle,
            title = it.title,
            originalLanguage = it.originalLanguage,
            backdrop = it.backdrop,
            rate = it.rate
        )
    }
}