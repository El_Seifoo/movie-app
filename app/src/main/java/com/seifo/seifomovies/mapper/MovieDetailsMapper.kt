package com.seifo.seifomovies.mapper

import com.seifo.seifomovies.model.model.Genre
import com.seifo.seifomovies.model.model.MovieDetails
import com.seifo.seifomovies.model.model.MovieCollection
import com.seifo.seifomovies.model.remote.*



fun RemoteMovieDetails.asMovieDetails(): MovieDetails {
    return this.let {
        MovieDetails(
            id = it.id,
            title = it.title,
            length = it.length,
            status = it.status,
            tagline = it.tagline,
            voteCount = it.voteCount,
            popularity = it.popularity,
            budget = it.budget,
            revenue = it.revenue,
            genres = it.remoteGenres?.asGenresList(),
            collection = it.remoteMovieCollection?.asMovieCollection(),
            releaseDate = it.releaseDate,
            poster = it.poster,
            originalTitle = it.originalTitle,
            originalLanguage = it.originalLanguage,
            backDrop = it.backDrop,
            rate = it.rate,
            overview = it.overview
        )
    }
}

private fun List<RemoteGenre>.asGenresList(): List<Genre> {
    return this.map {
        Genre(
            id = it.id,
            name = it.name
        )
    }
}

private fun RemoteMovieCollection.asMovieCollection(): MovieCollection {
    return this.let {
        MovieCollection(
            id = it.id,
            title = it.title,
            poster = it.poster
        )
    }
}