package com.seifo.seifomovies.mapper

import com.seifo.seifomovies.model.model.Review
import com.seifo.seifomovies.model.model.ReviewerDetails
import com.seifo.seifomovies.model.remote.RemoteReview
import com.seifo.seifomovies.model.remote.RemoteReviewerDetails


fun List<RemoteReview>.asReviewsList(): List<Review> {
    return this.map {
        Review(
            id = it.id,
            reviewer = it.reviewer,
            reviewerDetails = it.reviewerDetails?.asReviewerDetails(),
            content = it.content,
            createdAt = it.createdAt,
            url = it.url
        )
    }
}

fun RemoteReviewerDetails.asReviewerDetails(): ReviewerDetails {
    return this.let {
        ReviewerDetails(
            name = it.name,
            username = it.username,
            profilePic = it.profilePic,
            rate = it.rate
        )
    }
}