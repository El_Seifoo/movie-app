package com.seifo.seifomovies.mapper

import com.seifo.seifomovies.model.model.Series
import com.seifo.seifomovies.model.remote.ApiListResponse
import com.seifo.seifomovies.model.remote.RemoteSeries

fun ApiListResponse<RemoteSeries>.asSeriesList(): ApiListResponse<Series> {
    return ApiListResponse(
        this.data?.asSeriesList()?.toMutableList(),
        null,
        this.totalPages,
        this.statusMessage,
        this.statusCode,
        this.isSuccess
    )
}

fun List<RemoteSeries>.asSeriesList(): List<Series> {
    return this.map {
        Series(
            id = it.id,
            title = it.title,
            poster = it.poster,
            overview = it.overview,
            releaseDate = it.releaseDate,
            genres = it.genres,
            originalTitle = it.originalTitle,
            backDrop = it.backDrop,
            originalLanguage = it.originalLanguage,
            rate = it.rate
        )
    }
}