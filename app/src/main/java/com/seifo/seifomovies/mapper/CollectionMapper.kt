package com.seifo.seifomovies.mapper

import com.seifo.seifomovies.model.model.CollectionDetails
import com.seifo.seifomovies.model.remote.RemoteCollectionDetails

fun RemoteCollectionDetails.asCollection(): CollectionDetails {
    return this.let {
        CollectionDetails(
            id = it.id,
            title = it.title,
            overview = it.overview,
            poster = it.poster,
            backdrop = it.backdrop,
            movies = it.remoteMovies.asMoviesList()
        )
    }
}