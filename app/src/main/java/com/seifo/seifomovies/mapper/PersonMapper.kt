package com.seifo.seifomovies.mapper

import com.seifo.seifomovies.model.model.Person
import com.seifo.seifomovies.model.remote.ApiListResponse
import com.seifo.seifomovies.model.remote.RemotePerson

fun ApiListResponse<RemotePerson>.asPersonList(): ApiListResponse<Person> {
    return ApiListResponse(
        this.data?.asPersonsList()?.toMutableList(),
        null,
        this.totalPages,
        this.statusMessage,
        this.statusCode,
        this.isSuccess
    )
}
fun List<RemotePerson>.asPersonsList(): List<Person> {
    return this.map {
        Person(
            id = it.id,
            name = it.name,
            department = it.department,
            popularity = it.popularity,
            photo = it.photo,
            gender = it.gender
        )
    }
}