package com.seifo.seifomovies.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.seifo.seifomovies.mapper.asCollection
import com.seifo.seifomovies.model.model.CollectionDetails
import com.seifo.seifomovies.retrofit.AppApi
import java.lang.Exception

class CollectionDetailsRepository {
    val collectionDetails = MutableLiveData<CollectionDetails>()

    suspend fun fetchCollectionDetails(collectionId: Int) {
        try {
            val apiResponse = AppApi.apiService.getCollectionDetails(collectionId, "en")
            collectionDetails.value = apiResponse.asCollection()
        } catch (e: Exception) {
            Log.e("Collection Details Exc", e.message.toString())
        }
    }
}