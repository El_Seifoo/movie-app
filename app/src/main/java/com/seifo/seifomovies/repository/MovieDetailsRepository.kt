package com.seifo.seifomovies.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.seifo.seifomovies.mapper.asCastList
import com.seifo.seifomovies.mapper.asMovieDetails
import com.seifo.seifomovies.mapper.asMoviesList
import com.seifo.seifomovies.mapper.asReviewsList
import com.seifo.seifomovies.model.model.Cast
import com.seifo.seifomovies.model.model.Movie
import com.seifo.seifomovies.model.model.MovieDetails
import com.seifo.seifomovies.model.model.Review
import com.seifo.seifomovies.retrofit.AppApi
import java.lang.Exception

class MovieDetailsRepository {
    val movieDetails = MutableLiveData<MovieDetails>()
    val similarMovies = MutableLiveData<List<Movie>>()
    val cast = MutableLiveData<List<Cast>>()
    val reviews = MutableLiveData<List<Review>>()

    suspend fun fetchMovieDetails(movieId: Int) {
        try {
            val apiResponse = AppApi.apiService.getMovieDetails(movieId, "en")
            movieDetails.value = apiResponse.asMovieDetails()
        } catch (e: Exception) {
            Log.e("Movie Details Exception", e.message.toString())
        }
    }

    suspend fun fetchSimilarMovies(movieId: Int) {
        try {
            val apiResponse = AppApi.apiService.getSimilarMovies(movieId, 1, "en")
            similarMovies.value = apiResponse.data!!.asMoviesList()
        } catch (e: Exception) {
            Log.e("Similar Exception", e.message.toString())
        }
    }

    suspend fun fetchCast(movieId: Int) {
        try {
            val apiResponse = AppApi.apiService.getMovieCast(movieId, "en")
            cast.value = apiResponse.data1!!.asCastList().filterIndexed { index, _ ->
                index < 10
            }
        } catch (e: Exception) {
            Log.e("Cast Exception", e.message.toString())
        }
    }

    suspend fun fetchReviews(movieId: Int) {
        try {
            val apiResponse = AppApi.apiService.getMovieReview(movieId, 1, "en")
            reviews.value = apiResponse.data!!.asReviewsList()
        } catch (e: Exception) {
            Log.e("Similar Exception", e.message.toString())
        }
    }
}