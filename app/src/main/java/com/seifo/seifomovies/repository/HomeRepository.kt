package com.seifo.seifomovies.repository


import com.seifo.seifomovies.retrofit.AppApi

class HomeRepository {

    suspend fun getPopularMovies() = AppApi.apiService.getPopularMovies(1, "en")
    suspend fun getUpComingMovies() = AppApi.apiService.getUpComingMovies(1, "en")
    suspend fun getOnAirSeries() = AppApi.apiService.getOnAirSeries(1, "en")
    suspend fun getPopularSeries() = AppApi.apiService.getPopularSeries(1, "en")
    suspend fun getPopularPersons() = AppApi.apiService.getPopularPersons(1, "en")


}