package com.seifo.seifomovies.repository

import com.seifo.seifomovies.retrofit.AppApi

class MoviesRepository {

    suspend fun getMovies(page: Int, lang: String, flag: Int) = when (flag) {
        0 -> AppApi.apiService.getPopularMovies(page, lang)
        else -> AppApi.apiService.getUpComingMovies(page, lang)
    }
}