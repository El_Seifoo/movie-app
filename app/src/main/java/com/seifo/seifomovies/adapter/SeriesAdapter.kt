package com.seifo.seifomovies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.seifo.seifomovies.databinding.SeriesItemBinding
import com.seifo.seifomovies.model.model.Series

class SeriesAdapter(private val clickListener: OnSeriesItemClickListener) :
    ListAdapter<Series, SeriesAdapter.ViewHolder>(SeriesDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    class ViewHolder private constructor(private val binding: SeriesItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(series: Series, clickListener: OnSeriesItemClickListener) {
            binding.series = series
            binding.clickListener = clickListener
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = SeriesItemBinding.inflate(layoutInflater)
                return ViewHolder(binding)
            }
        }
    }
}


class OnSeriesItemClickListener(val clickListener: (series: Series) -> Unit) {
    fun onSeriesClick(series: Series) = clickListener(series)
}

class SeriesDiffUtil : DiffUtil.ItemCallback<Series>() {
    override fun areItemsTheSame(oldItem: Series, newItem: Series): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Series, newItem: Series): Boolean {
        return oldItem.id == newItem.id
    }

}