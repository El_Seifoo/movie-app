package com.seifo.seifomovies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.seifo.seifomovies.databinding.MovieItemBinding
import com.seifo.seifomovies.model.model.Movie

class MoviesAdapter(private val clickListener: OnMovieClickListener) :
    ListAdapter<Movie, MoviesAdapter.ViewHolder>(MoviesDffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    class ViewHolder private constructor(private val binding: MovieItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(movie: Movie, clickListener: OnMovieClickListener) {
            binding.movie = movie
            binding.clickListener = clickListener
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = MovieItemBinding.inflate(inflater)
                return ViewHolder(binding)

            }
        }

    }

}

open class OnMovieClickListener(val clickListener: (movie: Movie) -> Unit) {
    fun onMovieClick(movie: Movie) = clickListener(movie)
}

class MoviesDffUtil : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.id == newItem.id
    }

}