package com.seifo.seifomovies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.seifo.seifomovies.databinding.PersonItemBinding
import com.seifo.seifomovies.model.model.Person

class PersonAdapter(private val clickListener: OnPersonClickListener) :
    ListAdapter<Person, PersonAdapter.ViewHolder>(PersonDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    class ViewHolder private constructor(private val binding: PersonItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(person: Person, clickListener: OnPersonClickListener) {
            binding.person = person
            binding.clickListener = clickListener
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = PersonItemBinding.inflate(inflater)
                return ViewHolder(binding)
            }
        }
    }
}

class OnPersonClickListener(val clickListener: (person: Person) -> Unit) {
    fun onPersonClick(person: Person) = clickListener(person)
}

class PersonDiffUtil : DiffUtil.ItemCallback<Person>() {
    override fun areItemsTheSame(oldItem: Person, newItem: Person): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Person, newItem: Person): Boolean {
        return oldItem.id == newItem.id
    }

}