package com.seifo.seifomovies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.seifo.seifomovies.databinding.ReviewItemBinding
import com.seifo.seifomovies.model.model.Review

class ReviewsAdapter(private val clickListener: OnReviewClickListener) :
    ListAdapter<Review, ReviewsAdapter.ViewHolder>(ReviewDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    class ViewHolder private constructor(private val binding: ReviewItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(review: Review, clickListener: OnReviewClickListener) {
            binding.review = review
            binding.clickListener = clickListener
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = ReviewItemBinding.inflate(inflater)
                return ViewHolder(binding)
            }
        }
    }
}

class OnReviewClickListener(val clickListener: (review: Review) -> Unit) {
    fun onReviewClick(review: Review) = clickListener(review)
}

class ReviewDiffUtil : DiffUtil.ItemCallback<Review>() {
    override fun areItemsTheSame(oldItem: Review, newItem: Review): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Review, newItem: Review): Boolean {
        return oldItem.id == newItem.id
    }

}