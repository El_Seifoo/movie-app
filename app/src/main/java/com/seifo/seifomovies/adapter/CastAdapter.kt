package com.seifo.seifomovies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.seifo.seifomovies.databinding.CastItemBinding
import com.seifo.seifomovies.model.model.Cast

class CastAdapter(private val clickListener: OnCastClickListener) :
    ListAdapter<Cast, CastAdapter.ViewHolder>(CastDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    class ViewHolder private constructor(private val binding: CastItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(cast: Cast, clickListener: OnCastClickListener) {
            binding.cast = cast
            binding.clickListener = clickListener
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = CastItemBinding.inflate(inflater)
                return ViewHolder(binding)
            }
        }
    }
}

class OnCastClickListener(val clickListener: (cast: Cast) -> Unit) {
    fun onCastClick(cast: Cast) = clickListener(cast)
}

class CastDiffUtil : DiffUtil.ItemCallback<Cast>() {
    override fun areItemsTheSame(oldItem: Cast, newItem: Cast): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Cast, newItem: Cast): Boolean {
        return oldItem.id == newItem.id
    }

}