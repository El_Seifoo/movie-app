<?xml version="1.0" encoding="utf-8"?>
<layout>

    <data>

        <variable
            name="series"
            type="com.seifo.seifomovies.model.model.Series" />

        <variable
            name="clickListener"
            type="com.seifo.seifomovies.adapter.OnSeriesItemClickListener" />

    </data>

    <androidx.cardview.widget.CardView xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:onClick="@{ () -> clickListener.onSeriesClick(series)}"
        app:cardBackgroundColor="@color/dark_grey"
        app:cardCornerRadius="@dimen/_5sdp"
        app:cardElevation="@dimen/_2sdp"
        app:cardUseCompatPadding="true">

        <androidx.constraintlayout.widget.ConstraintLayout
            android:layout_width="wrap_content"
            android:layout_height="wrap_content">

            <ImageView
                android:id="@+id/series_poster"
                android:layout_width="@dimen/_100sdp"
                android:layout_height="@dimen/_130sdp"
                android:layout_marginStart="@dimen/_2sdp"
                android:layout_marginTop="@dimen/_2sdp"
                android:layout_marginEnd="@dimen/_2sdp"
                android:contentDescription="@string/movie_poster"
                android:scaleType="fitXY"
                app:img_size="@{@string/poster_w154}"
                app:img_url="@{series.poster}"
                app:layout_constraintEnd_toEndOf="@id/frame"
                app:layout_constraintStart_toStartOf="@id/frame"
                app:layout_constraintTop_toTopOf="@id/frame"
                tools:src="@drawable/movie" />

            <androidx.constraintlayout.widget.Guideline
                android:id="@+id/start_guide_line"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:orientation="vertical"
                app:layout_constraintGuide_begin="@dimen/_8sdp" />

            <androidx.constraintlayout.widget.Guideline
                android:id="@+id/end_guide_line"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:orientation="vertical"
                app:layout_constraintGuide_end="@dimen/_8sdp" />

            <androidx.constraintlayout.widget.Guideline
                android:id="@+id/bottom_guide_line"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:orientation="horizontal"
                app:layout_constraintGuide_end="@dimen/_8sdp" />

            <TextView
                android:id="@+id/series_name"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:ellipsize="end"
                android:gravity="center_vertical"
                android:maxLines="1"
                android:text="@{series.title}"
                android:textColor="@color/white"
                android:textSize="@dimen/_11ssp"
                app:layout_constraintEnd_toEndOf="@id/end_guide_line"
                app:layout_constraintStart_toStartOf="@id/start_guide_line"
                app:layout_constraintTop_toBottomOf="@id/series_poster"
                tools:text="Kingsman: the special forces" />

            <ImageView
                android:id="@+id/series_rate_img"
                android:layout_width="@dimen/_12sdp"
                android:layout_height="@dimen/_12sdp"
                android:layout_marginTop="@dimen/_4sdp"
                android:contentDescription="@string/rate_icon"
                android:src="@android:drawable/star_on"
                app:layout_constraintBottom_toBottomOf="@id/bottom_guide_line"
                app:layout_constraintEnd_toStartOf="@id/series_rate"
                app:layout_constraintStart_toStartOf="@id/start_guide_line"
                app:layout_constraintTop_toBottomOf="@id/series_name"
                app:rate_with_visibility="@{series.rate}"
                app:tint="@color/orange" />

            <TextView
                android:id="@+id/series_rate"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginStart="@dimen/_2sdp"
                android:ellipsize="end"
                android:maxLines="1"
                android:textSize="@dimen/_10ssp"
                android:textStyle="normal|bold"
                app:layout_constraintBottom_toBottomOf="@id/series_rate_img"
                app:layout_constraintStart_toEndOf="@id/series_rate_img"
                app:layout_constraintTop_toTopOf="@id/series_rate_img"
                app:rate_with_visibility="@{series.rate}"
                tools:text="7.5" />

            <androidx.constraintlayout.widget.Barrier
                android:id="@+id/series_name_barrier"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                app:barrierDirection="bottom"
                app:constraint_referenced_ids="series_name" />

            <TextView
                android:id="@+id/release_date"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginStart="@dimen/_6sdp"
                android:layout_marginTop="@dimen/_4sdp"
                android:ellipsize="middle"
                android:gravity="center|end"
                android:singleLine="true"
                android:textColor="@color/white"
                android:textSize="@dimen/_10ssp"
                app:date_format_with_visibility="@{series.releaseDate}"
                app:is_short="@{true}"
                app:layout_constraintBottom_toBottomOf="@id/bottom_guide_line"
                app:layout_constraintEnd_toEndOf="@id/end_guide_line"
                app:layout_constraintStart_toEndOf="@id/series_rate"
                app:layout_constraintTop_toBottomOf="@id/series_name_barrier"
                tools:text="31-12-2021" />

            <androidx.constraintlayout.widget.Barrier
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                app:barrierDirection="top"
                app:barrierMargin="@dimen/_8sdp"
                app:constraint_referenced_ids="series_rate_img,release_date" />

            <FrameLayout
                android:id="@+id/frame"
                android:layout_width="0dp"
                android:layout_height="0dp"
                android:background="@drawable/light_grey_frame"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent" />

        </androidx.constraintlayout.widget.ConstraintLayout>


    </androidx.cardview.widget.CardView>
</layout>
